<?php

namespace Drupal\uswds_facets_widgets\Plugin\facets\widget;

use Drupal\facets\Plugin\facets\widget\CheckboxWidget;
use Drupal\facets\FacetInterface;

/**
 * The uswds checkbox widget.
 *
 * @FacetsWidget(
 *   id = "uswds_checkbox",
 *   label = @Translation("List of checkboxes (USWDS Override)"),
 *   description = @Translation("A configurable widget that shows a list of checkboxes using the USWDS checkbox component"),
 * )
 */
class UswdsCheckboxWidget extends CheckboxWidget  {
  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);
    $build['#attached']['library'][] = 'uswds_facets_widgets/uswds_checkbox_widget';
    return $build;
  }

}
